/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: CategoricalPlaceIt
 * Description: This class is meant to handle and contain the name, which category, and the latitude/longitude
 *              of a categorical place-it.  It contains setter methods in order to provide information for 
 *              categorical place-its, and contains getter methods to retreive information of categorical 
 *              place-its.
 */


package com.example.googlemapdemo;
import java.util.ArrayList;
import java.util.List;
import com.google.android.gms.maps.model.LatLng;
import android.os.Parcel;
import android.os.Parcelable;
public class CategoricalPlaceIt extends AbstractPlaceIt implements Parcelable{
	private String name;
	private String[] categories;
	private List latList;
	private List lngList;
	private List placeList;
	private List addressList;
	
	public CategoricalPlaceIt() {
		latList = new ArrayList();
		lngList = new ArrayList();
		placeList = new ArrayList();
		addressList = new ArrayList();
	};
	
	public CategoricalPlaceIt(Parcel in) {
		super.setTitle(in.readString());
		super.setDescription(in.readString());
		super.setDay(in.readInt());
		super.setHour(in.readInt());
		super.setMinute(in.readInt());
		in.readStringArray(categories);
	}
	public CategoricalPlaceIt(String t, String d, int day, int hour, int min, String[] c){
		super.setTitle(t);
		super.setDescription(d);
		super.setDay(day);
		super.setHour(hour);
		super.setMinute(min);
		this.setCategory(c);
	}
	
	public void addLat(double lat){
		latList.add(lat);
	}
	public void addPlace(String place){
		placeList.add(place);
	}
	public void addLng(double lng){
		lngList.add(lng);
	}
	public void addAddress(String address){
		addressList.add(address);
	}
	public String [] getCategories(){
		return categories;
	}
	/*
	 * Implement Parcelable methods and 
	 * instantiate CREATOR object to serialize 
	 * PlaceIts objects into byte arrays
	 */
	public static final Parcelable.Creator<CategoricalPlaceIt> CREATOR
	    = new Parcelable.Creator<CategoricalPlaceIt>() {
	public CategoricalPlaceIt createFromParcel(Parcel in) {
	    return new CategoricalPlaceIt(in);
	}
	
	public CategoricalPlaceIt[] newArray(int size) {
	    return new CategoricalPlaceIt[size];
	}
	};
	public void setCategory(String [] categories){
		this.categories = categories;
	}
	
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public int getSize(){
		return latList.size();
	}
	public double getLat(int i){
		return (Double) latList.get(i);
	}
	public double getLng(int i){
		return (Double) lngList.get(i);
	}
	public String getPlace(int i){
		return (String)placeList.get(i);
	}
	public String getAddress(int i){
		return (String)addressList.get(i);
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int arg1) {
		out.writeString(super.getTitle());
		out.writeString(super.getDescription());
		out.writeInt(super.getDay());
		out.writeInt(super.getHour());
		out.writeInt(super.getMinute());
		out.writeStringArray(categories);
		out.writeList(latList);
		out.writeList(lngList);
		out.writeList(placeList);
	}
}