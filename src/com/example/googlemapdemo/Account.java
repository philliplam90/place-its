/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: Account
 * Description: This class is designed to hold the user name and password of different accounts.  It will
 *              also retreive the place-it information of that account, and it is also in charge of re-creating
 *              the place-it on the map when the account is logged in.
 */

package com.example.googlemapdemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class Account {
	
	private String userName;
	private String password;
	private String jsonString = null;
	private List<LocationPlaceIt> lPlaces = new ArrayList<LocationPlaceIt>();
	private List<CategoricalPlaceIt> cPlaces = new ArrayList<CategoricalPlaceIt>();
	private List<AbstractPlaceIt> abstractPlaceIts = new ArrayList<AbstractPlaceIt>();
	
	
	public Account(String name, String pass, String jstring){
		setUserName(name);
		setPassword(pass);
		this.jsonString = jstring;
	}
	
	//TODO remove placeits
	public void addLocationPlaceIt(LocationPlaceIt l){
		lPlaces.add(l);
	}
	
	public void addCategoricalPlaceIt(CategoricalPlaceIt c){
		cPlaces.add(c);
	}
	
	public void addAbstractPlaceIt(AbstractPlaceIt a){
		abstractPlaceIts.add(a);
	}
	
	public void setUserName(String u){
		this.userName = u;
	}
	
	public void setPassword(String p){
		this.password = p;
	}
	
	/*
	 * Location-Based PlaceIt String format:
	 * 
	 * name:description:day:lat:long; next...
	 * 
	 * Categorical PlaceIt String format:
	 * 
	 * name:description:day:category;
	 * 
	 * 
	 * LocationBasedPlaceIts%CategoricalPlaceIts
	 * 
	 */

	@Override
	public String toString(){
		String c = ":";
		String piece1 = "";
		String piece2 = "";
		String placeItString = "";
		
		
		//Construct String pieces to be parsed
		for (LocationPlaceIt l : lPlaces){
			
			piece1 += l.getTitle() + c + l.getDescription() + c + l.getDay() + c + l.getLatitude() + c + l.getLongitude() + ";";
		}
		//TODO Check for null categories during retrieval
		for (CategoricalPlaceIt x : cPlaces){
			piece2 += x.getTitle() + c + x.getDescription() + c + x.getDay() + c + x.getCategories()[0] + c + x.getCategories()[1] + c + x.getCategories()[2] + ";";
		}
		
		//Use % to parse locations from category place-its
		placeItString = piece1 + "%" + piece2;
		
		return placeItString;
	}
	
	// method to remake the place-its using the string stored online
	public void remakePlaceIts() {
		String placeitsstring = jsonString;

		String[] location_placeits = null;
		String[] categorical_placeits = null;
		String[] location_categorical_split = null;
		Log.e("Account JsonSTring", "?" + placeitsstring);
		if (placeitsstring == null) return;
		
		location_categorical_split = placeitsstring.split("%");

		
		if (placeitsstring.charAt(0) == '%' || placeitsstring.charAt(placeitsstring.length()-1) == '%') {
			if (placeitsstring.charAt(0) == '%') {
				// categorical placeits
				Log.d("CAT ONLY", placeitsstring);
				categorical_placeits = location_categorical_split[1].split(";");
				Log.e("cplaceits", Arrays.toString(categorical_placeits));
				for (int index = 0; index < categorical_placeits.length; index++) {
					String[] oneplaceitsinfo = categorical_placeits[index].split(":");
					Log.e("cplaceits", Arrays.toString(oneplaceitsinfo));
					Log.d("One Categorical Place it", Arrays.toString(oneplaceitsinfo));
					String title = oneplaceitsinfo[0];
					String description = oneplaceitsinfo[1];
					int day = Integer.parseInt(oneplaceitsinfo[2]);
					String cat1 = oneplaceitsinfo[3];
					String cat2 = oneplaceitsinfo[4];
					String cat3 = oneplaceitsinfo[5];
					String[] cats = {cat1, cat2, cat3};
					// call to create a place it
					// add it to "abstractPlaceIts"
					cPlaces.add(new CategoricalPlaceIt(title, description, day, 0, 0, cats));
					abstractPlaceIts.add(new CategoricalPlaceIt(title, description, day, 0, 0, cats));
					
				}
				
			}
			else {
				// location placeits
				Log.d("LOCATION ONLY", placeitsstring);
				location_placeits = location_categorical_split[0].split(";");
				for (int index = 0; index < location_placeits.length; index++) 
				{
					String[] oneplaceitsinfo = location_placeits[index].split(":");
					Log.d("One Location Place it", Arrays.toString(oneplaceitsinfo));
					String title = oneplaceitsinfo[0];
					String description = oneplaceitsinfo[1];
					int day = Integer.parseInt(oneplaceitsinfo[2]);
					double latitude = Double.parseDouble(oneplaceitsinfo[3]);
					double longitude = Double.parseDouble(oneplaceitsinfo[4]);
					
					// call to create a place it
					// add it to "abstractPlaceIts" and locationPlaceIts list
					lPlaces.add(new LocationPlaceIt(title, description, day, 0, 0, latitude, longitude));
					abstractPlaceIts.add(new LocationPlaceIt(title, description, day, 0, 0, latitude, longitude));
					
				}
			}


		}
		
		
		else if (location_categorical_split.length > 1) {
			Log.d("CAT & LOC PLACEITS", "found categorical and location placeits");
			Log.d("Split Placeit Types", Arrays.toString(location_categorical_split));
			
			location_placeits = location_categorical_split[0].split(";");
			Log.d("Split location", Arrays.toString(location_placeits));
			
			categorical_placeits = location_categorical_split[1].split(";");
			Log.d("Split Categorical", Arrays.toString(categorical_placeits));
		
			//if (location_placeits.length < 1) location_placeits = null;
			//if (categorical_placeits.length < 1) categorical_placeits = null;
			
			// remake location placeits
			if (location_placeits != null) {
				for (int index = 0; index < location_placeits.length; index++) 
				{
					String[] oneplaceitsinfo = location_placeits[index].split(":");
					Log.d("One Location Place it", Arrays.toString(oneplaceitsinfo));
					String title = oneplaceitsinfo[0];
					String description = oneplaceitsinfo[1];
					int day = Integer.parseInt(oneplaceitsinfo[2]);
					double latitude = Double.parseDouble(oneplaceitsinfo[3]);
					double longitude = Double.parseDouble(oneplaceitsinfo[4]);
					
					// call to create a place it
					// add it to "abstractPlaceIts" and locationPlaceIts list
					lPlaces.add(new LocationPlaceIt(title, description, day, 0, 0, latitude, longitude));
					abstractPlaceIts.add(new LocationPlaceIt(title, description, day, 0, 0, latitude, longitude));
					
				}
			}
			
			
			// remake categorical placeits
			if (categorical_placeits != null) {
				for (int index = 0; index < categorical_placeits.length; index++) {
					String[] oneplaceitsinfo = categorical_placeits[index].split(":");
					Log.d("One Categorical Place it", Arrays.toString(oneplaceitsinfo));
					String title = oneplaceitsinfo[0];
					String description = oneplaceitsinfo[1];
					String cat1 = null;
					String cat2 = null;
					String cat3 = null;
					int day = Integer.parseInt(oneplaceitsinfo[2]);
					if (oneplaceitsinfo.length > 3) {
						cat1 = oneplaceitsinfo[3];
						if (oneplaceitsinfo.length > 4) {
							cat2 = oneplaceitsinfo[4];
							if (oneplaceitsinfo.length > 5) {
									cat3 = oneplaceitsinfo[5];
							}
						}
					}
					//String cat3 = oneplaceitsinfo[5];
					String[] cats = {cat1, cat2, cat3};
					// call to create a place it
					// add it to "abstractPlaceIts"
					cPlaces.add(new CategoricalPlaceIt(title, description, day, 0, 0, cats));
					abstractPlaceIts.add(new CategoricalPlaceIt(title, description, day, 0, 0, cats));
					
				}
			}
		}
	}
	
	public List<AbstractPlaceIt> getPlaceIts () {
		return abstractPlaceIts;
	}
	
	public List<LocationPlaceIt> getLocationPlaceIts(){
		return lPlaces;
	}
	
	public List<CategoricalPlaceIt> getCategoricalPlaceIts(){
		return cPlaces;
	}
	
	public String getUsername () {
		return userName;
	}
	
	public String getPassword () {
		return password;
	}
	
	public String getJsonString() {
		return jsonString;
	}
	
	
	/*
	// method to delete a placeit
	public void deletePlaceIt (AbstractPlaceIt to_delete) {
		for (AbstractPlaceIt indexplaceit : abstractPlaceIts) {
			if (to_delete.getTitle().toString().equals(indexplaceit.getTitle().toString())) {
				// delete indexplaceit
				abstractPlaceIts.remove(indexplaceit);
			}
		}
		
	}
	*/
	
	
	public void updateData2 (final String user, final String pass, final String description) {
		String list[] = new String [3];
		list[0] = user;
		list[1] = pass;
		list[2] = description;
		FindUserTask fut = new FindUserTask();
		fut.execute(list);
	}
	
	
	//public void updateData (final String user, final String pass, final String description) {
		
		public class FindUserTask extends AsyncTask<String, Void, List<String>> {
			@Override
			protected List<String> doInBackground(String... acctinfo) {
				// TODO: attempt authentication against a network service.
				HttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet("http://gaelab110task2.appspot.com/product");
				//List<String> nameList = new ArrayList<String>();
				//List<String> passList = new ArrayList<String>();
		 
				try {
					HttpResponse response = client.execute(request);
					HttpEntity entity = response.getEntity();
					String data = EntityUtils.toString(entity);
					Log.d("Error", data);
					JSONObject myjson;
					// Simulate network access.
					try {
						myjson = new JSONObject(data);
						JSONArray array = myjson.getJSONArray("data");
						for (int i = 0; i < array.length(); i++) {
							JSONObject obj = array.getJSONObject(i);
							
							String user_pass = acctinfo[0]+ ":" + acctinfo[1];
							if (obj.get("name").toString().equals(user_pass)) {
								Log.d("FOUND", "Found the user!");
								postData(user_pass, acctinfo[2]);
								
							}
							
						}
						
					} catch (JSONException e) {
						
				    	Log.d("Error", "Error in parsing JSON");
				    	return null;
					}
					
				} 
				catch (ClientProtocolException e) {
					
			    	Log.d("Error", "ClientProtocolException while trying to connect to GAE");
				} catch (IOException e) {
		
					Log.d("Error", "IOException while trying to connect to GAE");
				}
				
				return null;
				
			}
			/*@Override
			protected void onPostExecute(List<String> list) {
				mAuthTask = null;
				showProgress(false);
				if (list != null) {
					CREDENTIALS = (ArrayList<String>) list;
			    	 for(String temp : CREDENTIALS)
			    	 {
			    		 Log.d(temp, "CREDENTIALS");
			    	 }
					finish();
				} else {
					currentPassView
							.setError(getString(R.string.error_incorrect_password));
					currentPassView.requestFocus();
				}
			}
			@Override
			protected void onCancelled() {
				mAuthTask = null;
				showProgress(false);
			}*/
		}
	//}
		
		
		
		private void postData(final String name_password, final String description) {
			//final ProgressDialog dialog = ProgressDialog.show(this,
			//		"Creating Account...", "Please wait...", false);
			Log.d("POSTDATA", "Attempting to post the Place-It");
			Thread t = new Thread() {

				public void run() {
					HttpClient client = new DefaultHttpClient();
					HttpPut put = new HttpPut("http://gaelab110task2.appspot.com/product");
					
				    try {
				    	
				    	//Create string for test placeit description
				    	
				      List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
				      nameValuePairs.add(new BasicNameValuePair("name",
				    		  name_password));//user+":"+pass));
				      nameValuePairs.add(new BasicNameValuePair("description",
				      		  description));
				      Log.d("NVP", "adding nameValuePair");
				     
				      nameValuePairs.add(new BasicNameValuePair("action",
					          "put"));
				      
				      put.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				    		      
				      
				      HttpResponse response = client.execute(put);
				      BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				      String line = "";
				      while ((line = rd.readLine()) != null) {
				        Log.d("TAG", line);
				      }

				    } catch (IOException e) {
				    	Log.d("TAG", "IOException while trying to conect to GAE");
				    }
					//dialog.dismiss();
				}
			};
			

			t.start();
			//dialog.show();
		}
		
		// method to delete a placeit
		public void deleteLocationPlaceIt (AbstractPlaceIt to_delete, boolean isLocationPlaceIt) {
			for (AbstractPlaceIt indexplaceit : abstractPlaceIts) {
				if (to_delete.getTitle().toString().equals(indexplaceit.getTitle().toString())) {
					// delete indexplaceit
					Log.d("INDEX PI", indexplaceit.toString());
					if (isLocationPlaceIt == true) {
						lPlaces.remove(to_delete);
						Log.d("LOC", "removed location placeit");
					}
					else { // removing a categorical placeit
						cPlaces.remove(to_delete);
						Log.d("CAT", "removed categorical placeit");
					}
					Log.d("lPlaces", "lPlaces is now : " + lPlaces.toString());
					Log.d("cPlaces", "cPlaces is now : " + cPlaces.toString());
					abstractPlaceIts.remove(to_delete);
					
					Log.d("TO STRING", this.toString());
					updateData2(this.userName, this.password, this.toString());
					
				}
			}
			
		}
		
		
		public void deleteCategoricalPlaceIt (CategoricalPlaceIt to_delete) {
			int i = 0;
			for (AbstractPlaceIt indexplaceit : abstractPlaceIts) {
				if (to_delete.getTitle().toString().equals(indexplaceit.getTitle().toString())) {
					// delete indexplaceit
					Log.d("CATEGORICAL", "REMOVING A CATEGORICAL PLACEIT");
					Log.d("INDEX PI", indexplaceit.toString());
					
					cPlaces.remove(i);
					Log.d("CAT", "removed categorical placeit");
				
					Log.d("cPlaces", "cPlaces is now : " + cPlaces.toString());
					abstractPlaceIts.remove(i);
					
					Log.d("TO STRING", this.toString());
					updateData2(this.userName, this.password, this.toString());
					
				}
				i++;
			}
			
		}
	
}