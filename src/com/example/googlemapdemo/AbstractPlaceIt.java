/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: AbstractPlaceIt
 * Description: This class is designed to utilize the observer pattern.  The Observer class is
 *              PlaceItObserver, and the subject class is PlaceItSubject.  This class is the template
 *              for the classes CategoricalPlaceit and LocationPlaceit, both of which will extend
 *              this class in order to implement its methods.
 */


package com.example.googlemapdemo;
import java.util.List;

import android.os.Parcelable;

public abstract class AbstractPlaceIt implements PlaceItSubject, Parcelable{
	private List <PlaceItObserver> observers;
	private int dayOfWeek;
	private int hour;
	private int minute;
	private String title;
	private String description;
	private boolean tempCallFlag = true; //temporary until we set 45min callback on placeit
										 // checks if placeit has already been shown to user
	public AbstractPlaceIt(){};
	public AbstractPlaceIt(String title, String description, int dayOfWeek, int hour, int minute){	
		this.title = title;
		this.description = description;
		this.dayOfWeek = dayOfWeek;
		this.hour = hour;
		this.minute = minute;
	};

	public void setDay(int dayOfTheWeek){
		this.dayOfWeek = dayOfTheWeek;
	}
	public void setHour(int hour){
		this.hour = hour;
	}
	public void setMinute(int minute){
		this.minute = minute;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public void setDescription(String description){
		this.description = description;
	}
	public void setCallFlag(boolean c){
		tempCallFlag = c;
	}
	public int getDay(){
		return dayOfWeek;
	}
	public int getHour(){
		return hour;
	}
	public int getMinute(){
		return minute;
	}
	public String getTitle(){
		return title;
	}
	public String getDescription(){
		return description;
	}
	public boolean getCallFlag(){
		return tempCallFlag;
	}
	@Override
	public void registerObserver(PlaceItObserver obs) {
		observers.add(obs);
	}
	@Override
	public void removeObserver(PlaceItObserver obs) {
		observers.remove(obs);
	}
	@Override
	public void notifyObservers() {
		// TODO Auto-generated method stub
	}
}