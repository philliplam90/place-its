/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: MySQLHelper
 * Description: This class is designed to be a helper class for the SQL database.  
 */

package com.example.googlemapdemo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLHelper extends SQLiteOpenHelper{
    
    
    public static final String TABLE_NAME = "locations";
    
    public static final String ID_COL = "loc_id";
    public static final String TITLE = "loc_title";
    public static final String SNIPPET = "loc_snippet";
    public static final String LATITUDE = "loc_latitude";
    public static final String LONGITUDE = "loc_longitude";

    private static final int D_VERSION = 1;
    
    private static final String DB_NAME = "markerlocations.db";
    private static final String DB_CREATE = 
      "create table "+ TABLE_NAME + "(" 
        + ID_COL + " integer primary key autoincrement, "
        + TITLE + " text, " 
        + SNIPPET + " text, "
        + LATITUDE + " text, "
        + LONGITUDE + " text);"
    ;
    public MySQLHelper(Context context) {
        super(context, DB_NAME, null, D_VERSION);
    }

	@Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
        onCreate(db);
    }

    
}