/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: PlaceItObserver
 * Description: This class is the observer class in the observer pattern.  The concrete observer class 
 *              implementing this class will need to override its update method.
 */


package com.example.googlemapdemo;


public interface PlaceItObserver{
	public void update(String title, String description);
}
