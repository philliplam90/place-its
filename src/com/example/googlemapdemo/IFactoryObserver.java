package com.example.googlemapdemo;

public interface IFactoryObserver {
	public void update(AbstractPlaceIt pi);
}
