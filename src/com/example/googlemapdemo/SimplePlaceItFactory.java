/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: SimplePlaceItFactory
 * Description: This class implements the factory pattern and determines whether or not to create a
 *              location place it or a categorical place it.  Basically, this class handles the 
 *              instantiation of a new place it.
 */


package com.example.googlemapdemo;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
public class SimplePlaceItFactory implements AbstractFactory, IFactorySubject {
	   private List<IFactoryObserver> observerList;
	   private String latitude;
	   private String longitude;
	   private final String APIKEY = "AIzaSyBC-3I4oPQmTrBwB1yxlykYQDeQV0_YeEo"; 
	   private final int radius = 2000;
	   private String type;
	   private String [] categories;
	   private ArrayList<Place> places = new ArrayList<Place>();
	   private int typeCount;
	   private LocationPlaceIt lp;
	   private CategoricalPlaceIt cp;
	   private int i = 0;
	   private boolean finish;
	   
	   public SimplePlaceItFactory(){
		   observerList = new ArrayList();
	   }
	   
    //This creates a PlaceIt and adds to the MainActivity AbstractPlaceIt Array
	public void create(String title, String description, double lat, double lng, String [] category){
		categories = category;
		latitude = String.valueOf(lat);
		longitude = String.valueOf(lng);
		
		if(category == null){
			lp = new LocationPlaceIt();
			lp.setLatitude(lat);
			lp.setLongitude(lng);
			notifyObservers(lp);
		}
		else{
			cp = new CategoricalPlaceIt();
	        cp.setCategory(category);
	        finish = false;
        	synchronized(cp){
        		type = category[i];
        		Log.d("Entering factory", "before thread");
	        	new GetCurrentLocation().execute(latitude, longitude);
	        }
		}
	}
	@Override
	public void registerObserver(IFactoryObserver obs) {
		Log.d("SimplePlaceItFactory", obs.getClass().toString());
		observerList.add(obs);
	}
	@Override
	public void removeObserver(IFactoryObserver obs) {
		observerList.remove(obs);
		
	}
	@Override
	public void notifyObservers(AbstractPlaceIt pi) {
		for(int j = 0; j < observerList.size(); j++)
			 observerList.get(j).update(pi);
	}
	
	/*
	 * 
	 * 
	 * 
	*/
	public static Document loadXMLFromString(String xml) throws Exception {
	       DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	       DocumentBuilder builder = factory.newDocumentBuilder();
	       InputSource is = new InputSource(new StringReader(xml));
	       return builder.parse(is);
	   }
	   private class GetCurrentLocation extends AsyncTask<Object, String, Boolean> {
	       @Override
	       protected Boolean doInBackground(Object... myLocationObjs) {
	           if(null != latitude && null != longitude) {
	               return true;
	           } else {
	               return false;
	           }
	       }
	       @Override
	       protected void onPostExecute(Boolean result) {
	           super.onPostExecute(result);
	           assert result;
	           StringBuilder query = new StringBuilder();
		           query.append("https://maps.googleapis.com/maps/api/place/nearbysearch/xml?");
		           query.append("location=" +  latitude + "," + longitude + "&");
		           query.append("radius=" + radius + "&");
		           query.append("types=" + type + "&");
		           query.append("sensor=true&"); //Must be true if queried from a device with GPS
		           query.append("key=" + APIKEY);
		    
	           new QueryGooglePlaces().execute(query.toString());
	           
	       }
	   }
	   /**
	    * Based on: http://stackoverflow.com/questions/3505930
	    */
	   private class QueryGooglePlaces extends AsyncTask<String, String, String> {
	       @Override
	       protected String doInBackground(String... args) {
	           HttpClient httpclient = new DefaultHttpClient();
	           HttpResponse response;
	           String responseString = null;
	           try {
	               response = httpclient.execute(new HttpGet(args[0]));
	               StatusLine statusLine = response.getStatusLine();
	               if(statusLine.getStatusCode() == HttpStatus.SC_OK){
	                   ByteArrayOutputStream out = new ByteArrayOutputStream();
	                   response.getEntity().writeTo(out);
	                   out.close();
	                   responseString = out.toString();
	               } else {
	                   //Closes the connection.
	                   response.getEntity().getContent().close();
	                   throw new IOException(statusLine.getReasonPhrase());
	               }
	           } catch (ClientProtocolException e) {
	               Log.e("ERROR", e.getMessage());
	           } catch (IOException e) {
	               Log.e("ERROR", e.getMessage());
	           }
	           return responseString;
	       }
	       @Override
	       protected void onPostExecute(String result) {
	           super.onPostExecute(result);
	           Log.d("Entering factory", "during thread");
	           try {
	               Document xmlResult = loadXMLFromString(result);
	               NodeList nodeList =  xmlResult.getElementsByTagName("result");
	               for(int i = 0, length = nodeList.getLength(); i < length; i++) {
	                   Node node = nodeList.item(i);
	                   if(node.getNodeType() == Node.ELEMENT_NODE) {
	                       Element nodeElement = (Element) node;
	                       Node name = nodeElement.getElementsByTagName("name").item(0);
	                       Node vicinity = nodeElement.getElementsByTagName("vicinity").item(0);
	                       Node rating = nodeElement.getElementsByTagName("rating").item(0);
	                       Node reference = nodeElement.getElementsByTagName("reference").item(0);
	                       Node id = nodeElement.getElementsByTagName("id").item(0);
	                       Node geometryElement = nodeElement.getElementsByTagName("geometry").item(0);
	                       NodeList locationElement = geometryElement.getChildNodes();
	                       Element latLngElem = (Element) locationElement.item(1);
	                       Node lat = latLngElem.getElementsByTagName("lat").item(0);
	                       Node lng = latLngElem.getElementsByTagName("lng").item(0);
	                       float[] geometry =  {Float.valueOf(lat.getTextContent()),
	                               Float.valueOf(lng.getTextContent())};
	                       typeCount = nodeElement.getElementsByTagName("type").getLength();
	                       String[] types = new String[typeCount];
	                       for(int j = 0; j < typeCount; j++) {
	                           types[j] = nodeElement.getElementsByTagName("type").item(j).getTextContent();
	                       }
	                       cp.addAddress(vicinity.getTextContent());
	                       //place.setId(id.getTextContent());
	                       cp.addPlace(name.getTextContent());
	                       //if(null == rating) {
	                       //    place.setRating(0.0f);
	                       //} else {
	                       //    place.setRating(Float.valueOf(rating.getTextContent()));
	                       //}
	                       //place.setReference(reference.getTextContent());
	                       cp.addLat(geometry[0]);
	                       cp.addLng(geometry[1]);
	                       //place.setTypes(types);
	                       //places.add(place);
	                   }
	                   if(i == 3)
	                	   finish = true;
	               }
	           } catch (Exception e) {
	               Log.e("ERROR", e.getMessage());
	           }
	           if(i < categories.length){
	        	   Log.d("SPF", "i = "+ i);
	        	   type = categories [i];
	        	   i++;
	        	   new GetCurrentLocation().execute(latitude, longitude);
	           }
	           if(finish){
	        	   Log.d("SPF", "i = "+ i + " categories.length = "+ categories.length + " " + finish);
	        	   Log.d("SPF", "cp = "+cp.getPlace(0));
	        	   notifyObservers(cp);
	           }
	       }
	   }
	  
}