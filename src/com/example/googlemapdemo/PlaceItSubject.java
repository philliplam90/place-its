/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: PlaceItSubject
 * Description: This class is the subject class in the observer pattern.  The concrete subject class 
 *              implementing this class will need to override its three methods.
 */


package com.example.googlemapdemo;
public interface PlaceItSubject{
	public void registerObserver(PlaceItObserver obs);
	public void removeObserver(PlaceItObserver obs);
	public void notifyObservers();
}
