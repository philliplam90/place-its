/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: Place
 * Description: This class contains all the fields and methods necessary for the result of a places
 *              API call.  It contains information about the place such as name, latitude and longitude,
 *              and rating.  
 */


package com.example.googlemapdemo;

//http://www.mkyong.com/java/jaxb-hello-world-example/
/**
 * Represents a single Result of a places API call
 * https://developers.google.com/places/documentation/search
 */
public class Place {
    private String vicinity;
    private float[] geometry; //array(0 => lat, 1 => lng)
    private String id;
    private String name;
    private float rating;
    private String reference;
    private String[] types;

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public float[] getGeometry() {
        return geometry;
    }

    public void setGeometry(float[] geometry) {
        this.geometry = geometry;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }
}