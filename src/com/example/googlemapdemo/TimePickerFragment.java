/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: TimePickerFragment
 * Description: This fragment is designed to use the current time as the default option for the user. 
 *              It will also check what time the user selected.
 */


package com.example.googlemapdemo;
import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment
                            implements TimePickerDialog.OnTimeSetListener {
	private int hour;
	private int min;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
    	hour = view.getCurrentHour();
    	min = minute;

    }
    
}