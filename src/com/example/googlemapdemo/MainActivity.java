/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: MainActivity
 * Description: This activity is meant to handle the overall map functionality of the place-its application.
 *              This includes handling user input for example: what happens when the user clicks on the map.
 *              This activity also is in charge of mediating other activities.  For example, when the user
 *              long clicks on the map, it means that they would like to create a categorical place-it.  This
 *              activity is in charge of starting the create a place-it activity when that occurs.
 */


package com.example.googlemapdemo;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

@SuppressLint("ShowToast")
public class MainActivity extends FragmentActivity implements GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener,OnMapClickListener, OnMarkerClickListener,CancelableCallback, 
OnInfoWindowClickListener, LocationListener, OnMapLongClickListener, IFactoryObserver
{

	public static Account currentAccount;
	Calendar c = Calendar.getInstance();
	int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
	boolean notified=false;
	private GoogleMap mMap;
    MarkerDataSource data;
	private List<Marker> mMarkers = new ArrayList<Marker>();
	private List<AbstractPlaceIt> mPlaceIts = new ArrayList<AbstractPlaceIt>();
	private DrawerLayout mDrawerLayout;
	private ArrayAdapter<String> drawer_adapter;
	private ListView mDrawerList;
	private List<String> stringTitles = new ArrayList<String>(); 
	private ActionBarDrawerToggle mDrawerToggle; 
	private CharSequence mDrawerTitle; 
	private CharSequence mTitle; 
	private Intent trackIntent;
	private boolean firstServiceCall = true;
	private LatLng position;
	private String title;
	private String description;
	private String [] categories = new String [3];
	private SimplePlaceItFactory placeItFactory;
	
	private int catCount = 0;
	
	
	//Add the following fields to the MainActivity class. 
	//Most of this is just for calculating the interval of updates we want in milliseconds.
	private final static int
	CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	// Milliseconds per second
	private static final int MILLISECONDS_PER_SECOND = 1000;
	// Update frequency in seconds
	public static final int UPDATE_INTERVAL_IN_SECONDS = 1;
	// Update frequency in milliseconds
	private static final long UPDATE_INTERVAL =
	MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
	// The fastest update frequency, in seconds
	private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
	// A fast frequency ceiling in milliseconds
	private static final long FASTEST_INTERVAL =
	MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
	
	// Define an object that holds accuracy and frequency parameters
	LocationRequest mLocationRequest;
	boolean mUpdatesRequested;
	private SharedPreferences.Editor mEditor;
	private SharedPreferences mPrefs;
	private LocationClient mLocationClient;
	protected boolean placeItMade = false;
	
   /*
	* Called by Location Services when the request to connect the
	* client finishes successfully. At this point, you can
	* request the current location or start periodic updates
	*/
	 protected void onCreate(Bundle savedInstanceState) 
	 {
		 
		 super.onCreate(savedInstanceState);
		 placeItFactory = new SimplePlaceItFactory();
		 placeItFactory.registerObserver(this);
		 
		 //START LOGIN 
		 
		 Intent intent = new Intent(this, LoginActivity.class);
		 startActivityForResult(intent,4);
		 
		 
		 setContentView(R.layout.activity_main);
		 setUpMapIfNeeded();
		 mMap.setMyLocationEnabled(true);
		 mMap.setOnMapClickListener(this);
		 mMap.setOnMarkerClickListener(this);
		 mMap.setOnInfoWindowClickListener(this);
		 mMap.setOnMapLongClickListener(this);
		 
		 
		 mLocationRequest = LocationRequest.create();
		 mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		 // Set the update interval to 5 seconds
		 mLocationRequest.setInterval(UPDATE_INTERVAL);
		 // Set the fastest update interval to 1 second
		 mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
		 // Open the shared preferences
		 mPrefs = getSharedPreferences("SharedPreferences", Context.MODE_PRIVATE);
		 // Get a SharedPreferences editor
		 mEditor = mPrefs.edit();
		 // Start with updates turned on
		 mUpdatesRequested = true;
		 /*
		  * Create a new location client, using the enclosing class to
		  * handle callbacks.
		  */
		 mLocationClient = new LocationClient(this, this, this);
		 /* -- Navigation Drawer -- */
		 // Setup Navigation Drawer
		 mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		 mDrawerList = (ListView) findViewById(R.id.left_drawer);
		 // Set the adapter for the list view in the navigation drawer
		 drawer_adapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, stringTitles);
		 mDrawerList.setAdapter(drawer_adapter);
		 // Set the drawer list's click listener
		 mDrawerList.setOnItemClickListener(new DrawerItemClickListener());			 
		 // Navigation Drawer Open/Close Button
		 mTitle = mDrawerTitle = getTitle();
		 mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		 mDrawerList = (ListView) findViewById(R.id.left_drawer);
		 getActionBar().setDisplayHomeAsUpEnabled(true);
		 mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, 
							 				R.string.drawer_open, R.string.drawer_close) 
		 {
			 /** Called when a drawer has settled in a completely closed state. */
			 public void onDrawerClosed(View view) {
				 super.onDrawerClosed(view);
				 getActionBar().setTitle(mTitle);
				 invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			 }
			 /** Called when a drawer has settled in a completely open state. */
			 public void onDrawerOpened(View drawerView) {
				 super.onDrawerOpened(drawerView);
				 getActionBar().setTitle(mDrawerTitle);
				 invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			 }
		 };
		 // Set the drawer toggle as the DrawerListener
		 mDrawerLayout.setDrawerListener(mDrawerToggle);	
		 getAllMarkers();
	 }
	 
	 
	 
	 private void getAllMarkers() {
	// get all markers from db
		 data = new MarkerDataSource(this);
	        try {
	           data.open();
	           
	        } catch (Exception e) {
	            Log.i("hello", "hello");
	        }
	        
	        List<LocationPlaceIt> m = data.getMyMarkers();
	        for (int i = 0; i < m.size(); i++) {
	            double lat = m.get(i).getLatitude();
	            double lon = m.get(i).getLongitude();
	            
	            LatLng pos = new LatLng(lat, lon);
	            mMap.addMarker(new MarkerOptions()
	                    .title(m.get(i).getTitle())
	                    .snippet(m.get(i).getDescription())
	                    .position(pos)
	                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.yellow_square)));
	           //putMarkerInSideBar(m.get(i));
	        }
	 }


	/* Called whenever we call invalidateOptionsMenu() */
	 @Override
	 public boolean onPrepareOptionsMenu(Menu menu) {
		 // If the nav drawer is open, hide action items related to the content view
		 return super.onPrepareOptionsMenu(menu);
	 }
	 @Override
	 protected void onPostCreate(Bundle savedInstanceState) {
		 super.onPostCreate(savedInstanceState);
		 // Sync the toggle state after onRestoreInstanceState has occurred.
		 mDrawerToggle.syncState();
	 }
	 @Override
	 public void onConfigurationChanged(Configuration newConfig) {
		 super.onConfigurationChanged(newConfig);
		 mDrawerToggle.onConfigurationChanged(newConfig);
	 }
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
		 // Pass the event to ActionBarDrawerToggle, if it returns
		 // true, then it has handled the app icon touch event
		 if (mDrawerToggle.onOptionsItemSelected(item)) {
			 return true;
		 }
		 // Handle your other action bar items...
		 return super.onOptionsItemSelected(item);
	 }
	 
	 
	 public void geoLocate(View v) throws IOException
	 {
		 try
		 {
			 hideSoftKeyboard(v);
			 EditText editText = (EditText) findViewById(R.id.sendLocation);
			 String geoData = editText.getText().toString();
			 Geocoder gc = new Geocoder(this);
			 List<android.location.Address>list = gc.getFromLocationName(geoData,1);
			 android.location.Address add = list.get(0);
		     String locality = add.getLocality();
		     Toast.makeText(this, locality, Toast.LENGTH_LONG).show();
		     double lat = add.getLatitude();
		     double lng = add.getLongitude();
		     mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng), 12),2000,null);
	
	    }
		catch(IOException e1)
		{
		}
	    catch(IllegalStateException e)
	    {
	    }
		catch(IndexOutOfBoundsException e3)
		{
		}
	}
	 
	 private void hideSoftKeyboard(View v)
	 {
		 InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
		 imm.hideSoftInputFromWindow(v.getWindowToken(),0);
	 }
	
	 // Check if map is instantiated 
	 private void setUpMapIfNeeded() 
	 {
		// Do a null check to confirm that we have not already instantiated the map.
		 if (mMap == null) 
		 {
			 mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
			 .getMap();
			 // Check if we were successful in obtaining the map.
			 if (mMap != null) 
			 {
				 // The Map is verified. It is now safe to manipulate the map.
			 }
		 }
	 }
	 

	 // Click map to create Place-It
	 @Override
	 public void onMapClick(LatLng position) {
		 this.position = position;
		 Intent intent = new Intent(this, CreatePlaceItActivity.class);
		 startActivityForResult(intent, 0);
	 }
	 @Override
	 public void onMapLongClick(LatLng position) {
		 this.position = position;
		 Intent intent = new Intent(this, CreatePlaceItActivity.class);
		 startActivityForResult(intent, 1);
	 }
	 
	 //Collect results from external activities
	 // 0. Create LocationPlaceIt
	 // 1. Create CategoricalPlaceIt
	 @Override
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {
	   super.onActivityResult(requestCode, resultCode, data);
	   switch(requestCode) {
	     case (0) : {
	       if (resultCode == FragmentActivity.RESULT_OK) {
		        placeItMade = data.getBooleanExtra(CreatePlaceItActivity.PLACEITMADE, false);
				if(placeItMade){
					title = data.getStringExtra(CreatePlaceItActivity.TITLE);
					description = data.getStringExtra(CreatePlaceItActivity.DESCRIPTION);
					dayOfWeek = data.getIntExtra(CreatePlaceItActivity.DAYOFWEEK, 0);
					LocationPlaceIt pi = new LocationPlaceIt();
					pi.setLatitude(position.latitude);
					pi.setLongitude(position.longitude);
					pi.setTitle(title);
					pi.setDescription(description);
					pi.setDay(dayOfWeek);
					addPlaceItToMap(pi);
					
					String username = currentAccount.getUsername();
					String password = currentAccount.getPassword();
					String parsedDescription = description.split("\n")[0];
					
					AbstractPlaceIt newLplaceit = new LocationPlaceIt(title, parsedDescription, dayOfWeek, 0, 0, position.latitude, position.longitude);
					currentAccount.addLocationPlaceIt((LocationPlaceIt)newLplaceit);
					currentAccount.addAbstractPlaceIt(newLplaceit);
					
					
							
					currentAccount.updateData2(username, password, currentAccount.toString());
				}
	       }
	      break;
	     }
	     case (1) : {
	      if (resultCode == FragmentActivity.RESULT_OK) {
	    	  Intent intent = new Intent(this, PickCategoryActivity.class);
	 		  startActivityForResult(intent, 2);
	    	  placeItMade = data.getBooleanExtra(CreatePlaceItActivity.PLACEITMADE, false);
	    	  if(placeItMade){
			  	  title = data.getStringExtra(CreatePlaceItActivity.TITLE);
				  description = data.getStringExtra(CreatePlaceItActivity.DESCRIPTION);
				  dayOfWeek = data.getIntExtra(CreatePlaceItActivity.DAYOFWEEK, 0);
				  Toast.makeText(this, title, 1000).show();
				  
				  
			  }
	    	  mLocationClient.disconnect();
	      }
	      break;
	     }
	     //break;
	     case (2) : {
		      if (resultCode == FragmentActivity.RESULT_OK) {
		    	  categories[0] = data.getStringExtra(PickCategoryActivity.CATEGORY1);
		    	  categories[1] = data.getStringExtra(PickCategoryActivity.CATEGORY2);
		    	  categories[2] = data.getStringExtra(PickCategoryActivity.CATEGORY3);
		    	  Intent intent = new Intent(this, HttpTestActivity.class);
		    	  Intent intent2 = new Intent(intent);
		    	  Intent intent3 = new Intent(intent);
		    	  intent.putExtra("CATEGORY", categories[0]);
		    	  intent2.putExtra("CATEGORY", categories[1]);
		    	  intent3.putExtra("CATEGORY", categories[2]);
		    	  intent.putExtra("LATITUDE", position.latitude);
		    	  intent.putExtra("LONGITUDE", position.longitude);
		    	  intent2.putExtra("LATITUDE", position.latitude);
		    	  intent2.putExtra("LONGITUDE", position.longitude);
		    	  intent3.putExtra("LATITUDE", position.latitude);
		    	  intent3.putExtra("LONGITUDE", position.longitude);
		    	  placeItFactory.create(title, description, position.latitude, position.longitude, categories);
		    	  
		    	  
		      }
		     }
		     break;
	     case (3) : {
		      if (resultCode == FragmentActivity.RESULT_OK) {
		    	  
		    	  int placesSize = data.getIntExtra(HttpTestActivity.PLACESSIZE, 0);
		    	  CategoricalPlaceIt pi = new CategoricalPlaceIt();
		    	  
		    	  for(int i = 0; i < placesSize; i++){
		    		  float lat = data.getFloatExtra("Lat"+i,0);
		    		  float lng = data.getFloatExtra("Lng"+i,0);
		    		  String name = data.getStringExtra("Name"+i);
		    		  pi.addPlace(name);
		    		  pi.addLat(lat);
		    		  pi.addLng(lng);
		    	  }
		    	  pi.setCategory(categories);
		    	  addPlaceItToMap(pi);
		    	  
		    	  if (catCount == 2){
			    	  String username = currentAccount.getUsername();
					  String password = currentAccount.getPassword();
					  String parsedDescription = description.split("\n")[0];
					  
					  
					  AbstractPlaceIt newCplaceit = new CategoricalPlaceIt(title, parsedDescription, dayOfWeek, 0, 0, categories);
					  
					  
					  
					  currentAccount.addCategoricalPlaceIt((CategoricalPlaceIt)newCplaceit);			  
					  currentAccount.addAbstractPlaceIt(newCplaceit);
					  
					  Log.e("CATEGORICAL CREATED", categories[0] + categories[1] + categories[2]);
	
					  Log.e("CATEGORICAL ACCOUNT", currentAccount.getCategoricalPlaceIts().get(0).getCategories()[0] + currentAccount.getCategoricalPlaceIts().get(0).getCategories()[1] + currentAccount.getCategoricalPlaceIts().get(0).getCategories()[2]);
					  Log.e("ACCOUNT TOSTRING", currentAccount.toString());		
					  currentAccount.updateData2(username, password, currentAccount.toString());
					  catCount = 0;
					  Log.e("catCount", "" + catCount);
		    	  }
		    	  else catCount++;
		    	  
		      }
		      break;
		     }
	     case (4) : {
		    	// remake the place its when you return from LoginActivity
				 //Log.d("REMAKE1", "currentAccoutn.remakePlaceIts()");
		    	 currentAccount.remakePlaceIts();
		    	 //Log.d("REMAKE2", "remakeAccountPlaceIts()");
				 remakeAccountPlaceIts();
				 //Log.d("BREAK", "reached break statement");
				 break;
		     	}
	     case (5) : 
		    	 int placesSize = data.getIntExtra(HttpTestActivity.PLACESSIZE, 0);
	   	  		CategoricalPlaceIt pi = new CategoricalPlaceIt();
	   	  
	   	  		for(int i = 0; i < placesSize; i++){
		   		  float lat = data.getFloatExtra("Lat"+i,0);
		   		  float lng = data.getFloatExtra("Lng"+i,0);
		   		  String name = data.getStringExtra("Name"+i);
		   		  pi.addPlace(name);
		   		  pi.addLat(lat);
		   		  pi.addLng(lng);
	   	  		}
		   	  pi.setCategory(categories);
		   	  addPlaceItToMap(pi);
		}
	   
	   

	 }
	 
	 public void remakeAccountPlaceIts(){
		 
		 // Remake any location placeits
		 for (LocationPlaceIt l : currentAccount.getLocationPlaceIts()){
			 /*
			 mPlaceIts.add(l);
			 
			 Marker added = mMap.addMarker(new MarkerOptions()
			 .position(new LatLng(l.getLatitude(),l.getLongitude()))
			 .title(l.getTitle())
			 .snippet(l.getDescription())
			 .icon(BitmapDescriptorFactory.fromResource(R.drawable.yellow_square)));
			 mMarkers.add(added);	
			 stringTitles.add(l.getTitle());
			 drawer_adapter.notifyDataSetChanged();
			 */
			 
			 title = l.getTitle();
			 description = l.getDescription();
			 
			 addPlaceItToMap(l);
		 }
		 
		 // remake any categorical placeits
		 for (CategoricalPlaceIt p : currentAccount.getCategoricalPlaceIts()){
			  categories[0] = p.getCategories()[0];
	    	  categories[1] = p.getCategories()[1];
	    	  categories[2] = p.getCategories()[2];
	    	  description = p.getDescription();
	    	  title = p.getTitle();
	    	  dayOfWeek = p.getDay();
	    	  Intent intent = new Intent(this, HttpTestActivity.class);
	    	  Intent intent2 = new Intent(intent);
	    	  Intent intent3 = new Intent(intent);
	    	  intent.putExtra("CATEGORY", categories[0]);
	    	  intent2.putExtra("CATEGORY", categories[1]);
	    	  intent3.putExtra("CATEGORY", categories[2]);
	    	  /*
	    	  Location location=manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);//
	    	  location.getLatitude();
	    	  location.getLongitude();*/
	    	  
	    	  
	    	  String locationProvider = LocationManager.NETWORK_PROVIDER;
	    	// Or use LocationManager.GPS_PROVIDER	

	    	
	    	LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
			Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
			
	    	  double lat = lastKnownLocation.getLatitude();
	    	  double lng = lastKnownLocation.getLongitude();
	    	  Log.e("MyLat", ""+lat);
	    	  Log.e("MyLng", ""+lng);
	    	  intent.putExtra("LATITUDE", lat);
	    	  intent.putExtra("LONGITUDE", lng);
	    	  intent2.putExtra("LATITUDE", lat);
	    	  intent2.putExtra("LONGITUDE", lng);
	    	  intent3.putExtra("LATITUDE", lat);
	    	  intent3.putExtra("LONGITUDE", lng);
	    	  /*
	    		  startActivityForResult(intent, 5);
	    	  
	    		  startActivityForResult(intent2, 5);
	    	  
	    		  startActivityForResult(intent3, 5);
	    		  */
	    	  placeItFactory.create(title, description, lat, lng, categories);
		 }
		 
		 
		 if (mPlaceIts != null)
		 {
			//Start tracker service
			 trackIntent = new Intent(this, TrackerService.class);
			 ArrayList<AbstractPlaceIt> mm = new ArrayList<AbstractPlaceIt>(mPlaceIts);
			 trackIntent.putParcelableArrayListExtra("placeits", mm);
			 startService(trackIntent);
		 }
	 }
	 
	 public void addMarkerToMap(LocationPlaceIt pi, String name){
		 Marker added = mMap.addMarker(new MarkerOptions()
		 .position(new LatLng(pi.getLatitude(), pi.getLongitude()))
		 .title(title+" - "+name)
		 .snippet(description)
		 .icon(BitmapDescriptorFactory.fromResource(R.drawable.yellow_square)));
		 mMarkers.add(added);
		 
		 mPlaceIts.add(pi);
		 if (placeItMade)
		 {
			 trackIntent = new Intent(this, TrackerService.class);
			 if(!firstServiceCall) stopService(trackIntent);
			 
			 //store place-its inside intent object
			 
			 //Log.d("BEFORE put", "put");
			 ArrayList<AbstractPlaceIt> mm = new ArrayList<AbstractPlaceIt>(mPlaceIts);
			 trackIntent.putParcelableArrayListExtra("placeits", mm);
			
			 
			 //Log.d("AFTER put", "put");
			 startService(trackIntent);
			 
			 placeItMade = false;
		 }
	 }
	 
	 public void addPlaceItToMap(AbstractPlaceIt pi){
		 /* Add the Place-It's title to stringTitles, so it will appear in the
		 	Navigation Drawer */
		 stringTitles.add(title);
		 drawer_adapter.notifyDataSetChanged();
		 if(pi.getClass()==CategoricalPlaceIt.class){
			 for(int i = 0; i < ((CategoricalPlaceIt)pi).getSize();i++){
				 LocationPlaceIt lpi = new LocationPlaceIt();
				 lpi.setLatitude(((CategoricalPlaceIt)pi).getLat(i));
				 lpi.setLongitude(((CategoricalPlaceIt)pi).getLng(i));
				 lpi.setTitle(title);
				 lpi.setDescription(description);
				 lpi.setDay(dayOfWeek);
				 placeItMade = true;
				 addMarkerToMap(lpi, ((CategoricalPlaceIt)pi).getPlace(i));
			 }
		 }
		 if(pi.getClass()==LocationPlaceIt.class){
			 addMarkerToMap((LocationPlaceIt)pi, " ");
		 }
	 }
	 
	 @Override
	 public void onPause(){
		 super.onPause();
			// Save the current setting for updates
			mEditor.putBoolean("KEY_UPDATES_ON", mUpdatesRequested);
			mEditor.commit();
	 }
	 @Override
	protected void onStart() {
		super.onStart();
		mLocationClient.connect();
	}
	@Override
	protected void onResume() {
		super.onResume();
		/*
		* Get any previous setting for location updates
		* Gets "false" if an error occurs
		*/
		if (mPrefs.contains("KEY_UPDATES_ON")) {
			mUpdatesRequested =
			mPrefs.getBoolean("KEY_UPDATES_ON", false);
			// Otherwise, turn off location updates
		} else {
			mEditor.putBoolean("KEY_UPDATES_ON", false);
			mEditor.commit();
		}
	}

	 public void onDestroy(){
		super.onDestroy();
		stopService(trackIntent);
	 }
	 
	 @Override
	 public void onLocationChanged(Location location) {
		 //Toast.makeText(this, "location changed: mainactivity", Toast.LENGTH_LONG).show();
		}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
			/*
			* Google Play services can resolve some errors it detects.
			* If the error has a resolution, try sending an Intent to
			* start a Google Play services activity that can resolve
			* error.
			*/
			if (connectionResult.hasResolution()) {
				try {
					// Start an Activity that tries to resolve the error
					connectionResult.startResolutionForResult(
					this,
					CONNECTION_FAILURE_RESOLUTION_REQUEST);
					/*
					* Thrown if Google Play services canceled the original
					* PendingIntent
					*/
				} catch (IntentSender.SendIntentException e) {
					// Log the error
						e.printStackTrace();
					}
				} else {
				/*
				* If no resolution is available, display a dialog to the
				* user with the error.
				*/
				Toast.makeText(this, "FAILURE!", Toast.LENGTH_LONG).show();
				}
			
		}
		
		/* * Called by Location Services if the connection to the * location 
		 * client drops because of an error. */
		@Override
		public void onConnected(Bundle dataBundle) {
			Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
			// If already requested, start periodic updates
			if (mUpdatesRequested) {
			mLocationClient.requestLocationUpdates(mLocationRequest, this);
			}
			
		}

		@Override
		public void onDisconnected() {
			// TODO Auto-generated method stub
			Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
		}
	 public void onFinish() 
	 {
		 /* Nothing here */
	 }

	
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onInfoWindowClick(Marker arg0) {
		
		 AlertDialog.Builder alert = new AlertDialog.Builder(this);
		 alert.setTitle(arg0.getTitle());
	
		 //Create Linear Layout
		 Context context = this;
		 LinearLayout layout = new LinearLayout(context);
		 layout.setOrientation(LinearLayout.VERTICAL);
		 
		 //Set an EditText view to get user input: Title and Description 
		 final EditText titleBox = new EditText(this);
		 titleBox.setKeyListener(null);
		 titleBox.setHint(arg0.getSnippet());
		 layout.addView(titleBox);
		 alert.setView(layout);
		 alert.show();
		
	}
	
	/*DrawerItemClickListener: A Listener for when an item in the drawer is clicked*/
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int item_position, long id) {
        	mDrawerLayout.closeDrawers();
        	selectItem(item_position);
        }
    }
 
	/*selectItem: runs the activity specified by the item clicked.  In this case, it should
	 * 				display the contents of the Place-It */
	private void selectItem(int item_position) {
				Marker marker_called = mMarkers.get(item_position);
	            //onInfoWindowClick(marker_called);    
	            mMap.animateCamera(CameraUpdateFactory.newLatLng(marker_called.getPosition()), 200, (CancelableCallback) this);
	   		 	marker_called.showInfoWindow();
	  
	}

	/*showTimePickerDialog: displays the time picker to choose a notificationt time for Place-It */
	public void showTimePickerDialog(View v) {
	    DialogFragment newFragment = new TimePickerFragment();
	    newFragment.show(getFragmentManager(), "timePicker");
	}

		
	@Override
	public void onBackPressed()
	{
		Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(setIntent);
		return;
	}

	public boolean getNotified() {
		
		// TODO Auto-generated method stub
		return notified;
	}


	//when a marker is clicked
	@Override
	public boolean onMarkerClick(final Marker marker) {
		// TODO Auto-generated method stub
		
		//create a dialoge that contains marker info and option buttons.
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(marker.getTitle());
		alert.setMessage(marker.getSnippet());
		
		//create the discard button.
		alert.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
			 public void onClick(DialogInterface dialog, int whichButton) {
				 marker.remove();
				 
				 List<LocationPlaceIt> allData = data.getMyMarkers();
			        for (int i = 0; i < allData.size(); i++) {
			            String title = marker.getTitle();
			            String dataTitle = allData.get(i).getTitle();
			            
			            if(dataTitle.equals(title)){
			            	data.deleteMarker(allData.get(i));
			            	
			            }
			            
			        }
			        
			        
			        String markertitle[] = marker.getTitle().split("-");
			        boolean isLocPI = true;
			        if (markertitle[1] != null) {
	            		isLocPI = false;
	            	}
	            	//Log.e("SEARCH FOR", markertitle[0]+"_");
		            for (AbstractPlaceIt index : mPlaceIts) {
		            	//Log.e("for loop", "current pi title: " + index.getTitle()+"_");
		            	if (markertitle[0].toString().equals(index.getTitle() + " ")) {
		            		Log.e("FOUND MATCH", "found a match");
		            		//Log.e("THE MATCH", index.getTitle());
			            	mPlaceIts.remove(index);
			            	//currentAccount.setlocationPlaceIts();
			            	//currentAccount.setabstractPlaceIts(mPlaceIts);
			            	//currentAccount.updateData2(currentAccount.getUsername(), currentAccount.getPassword(), currentAccount.toString());
			            	Log.e("CLASS", index.getClass().toString());
			            	
			            	if (isLocPI == false) {
			            		String c[] = {"X", "X", "X"};
			            		CategoricalPlaceIt fauxCatPI = new CategoricalPlaceIt(index.getTitle(), index.getDescription(), index.getDay(), index.getHour(), index.getMinute(), c);
			            		currentAccount.deleteCategoricalPlaceIt(fauxCatPI);
			            	}
			            	
			            	
			            	/*
			            	if (index.getClass() == LocationPlaceIt.class) {
			            		isLocPI = true;
			            	}
			            	else {
			            		isLocPI = false;
			            	}*/
			            	else currentAccount.deleteLocationPlaceIt(index, isLocPI);
			            	
			            	try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			            	break;
		            	}
		            	
		            }
			 }
			 });
		//create the renew button.
		alert.setPositiveButton("Renew", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// renew the marker onclick.
				
			}
		});
		//show the dialogue
		alert.show();
		return true;
	}



	@Override
	public void update(AbstractPlaceIt pi) {
		addPlaceItToMap(pi);
		
	}


}