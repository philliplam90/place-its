/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: LocationPlaceIt
 * Description: This class is meant to extend Abstractplaceit.  It contains setters and getters in order to set
 *              information about the location place-it such as the name and description, and get information 
 *              about the place-it such as longitude and latitude.
 */


package com.example.googlemapdemo;

import android.os.Parcel;
import android.os.Parcelable;

public class LocationPlaceIt extends AbstractPlaceIt implements Parcelable{
	private double latitude;
	private double longitude;
	
	public LocationPlaceIt(){};
	public LocationPlaceIt(String title, String description, int dayOfWeek, int hour, int minute, double latitude, double longitude){
		super(title, description, dayOfWeek, hour, minute);
		this.latitude = latitude;
		this.longitude = longitude;
	};
	
	public LocationPlaceIt(Parcel in) {
		super.setTitle(in.readString());
		super.setDescription(in.readString());
		super.setDay(in.readInt());
		super.setHour(in.readInt());
		super.setMinute(in.readInt());
		latitude = in.readDouble();
		longitude = in.readDouble();
		
	}
	public double getLongitude(){
		return longitude;
	}
	public double getLatitude(){
		return latitude;
	}
	public void setLatitude(double lat){
		latitude = lat;
	}
	public void setLongitude(double lon){
		longitude = lon;
	}
	/*
	 * Implement Parcelable methods and 
	 * instantiate CREATOR object to serialize 
	 * PlaceIts objects into byte arrays
	 */
	public static final Parcelable.Creator<LocationPlaceIt> CREATOR
	    = new Parcelable.Creator<LocationPlaceIt>() {
	public LocationPlaceIt createFromParcel(Parcel in) {
	    return new LocationPlaceIt(in);
	}
	
	public LocationPlaceIt[] newArray(int size) {
	    return new LocationPlaceIt[size];
	}
	};

	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(super.getTitle());
		out.writeString(super.getDescription());
		out.writeInt(super.getDay());
		out.writeInt(super.getHour());
		out.writeInt(super.getMinute());
		out.writeDouble(latitude);
		out.writeDouble(longitude);
	}
}
