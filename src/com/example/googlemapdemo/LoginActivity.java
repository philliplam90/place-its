/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: LoginActivity
 * Description: This Activity is designed to handle the logging in feature of the place-its app.
 *              This activity is started right after mainactivity is started.  It will handle registering
 *              usernames and passwords by calling the register activity.  It will also handle valid and invalid
 *              user names and passwords, and go into the map upon successful login.
 */


package com.example.googlemapdemo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
//import com.google.api.client.http.HttpResponse;
//

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {
	/**
	 * Authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static ArrayList<String> CREDENTIALS = new ArrayList<String>();
	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;
	// Values for email and password at the time of the login attempt.
	private String currentUserName;
	private String currentPass;
	// UI references.
	private EditText currentUserNameView;
	private EditText currentPassView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	private Context context = this;
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	
		setContentView(R.layout.activity_login1);
		
		CREDENTIALS.add("foo@example.com:hello");
		CREDENTIALS.add("bar@example.com:world");
		
		
		// Set up the login form.
		currentUserName = getIntent().getStringExtra(EXTRA_EMAIL);
		currentUserNameView = (EditText) findViewById(R.id.email);
		currentUserNameView.setText(currentUserName);
		currentPassView = (EditText) findViewById(R.id.password);
		currentPassView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,	
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});
		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);
		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
		
		findViewById(R.id.register_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						registerDialog();
					}
				});
	}
	
	//Dialog to register a new Account
	public void registerDialog()
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		 alert.setTitle("Create Account");
		
		 
		 final EditText emailBox = new EditText(context);
	    final EditText passwordBox = new EditText(context);
		 final EditText reenterPasswordBox = new EditText(context); 
		 // Create Linear Layout
		 
		 final LinearLayout layout = new LinearLayout(context);
		 layout.setOrientation(LinearLayout.VERTICAL);
		 
		 // Set an EditText view to get user input: Title and Description 
		 
		 
		 emailBox.setHint("currentUserName");
		 layout.addView(emailBox);
		 
		 // edit description
		
		 passwordBox.setHint("Password");
		 passwordBox.setTransformationMethod(new PasswordTransformationMethod());
		 layout.addView(passwordBox);
		 
		 // edit description
		
		 reenterPasswordBox.setHint("Renter Password");
		 reenterPasswordBox.setTransformationMethod(new PasswordTransformationMethod());
		 layout.addView(reenterPasswordBox);
		 
		 alert.setView(layout);
		 
		 // Create button for account creation
		 alert.setPositiveButton("Create Account", new DialogInterface.OnClickListener() {
		 public void onClick(DialogInterface dialog, int whichButton) {
			 
			 
			 //Verify that passwords are equal
			 Log.d(passwordBox.getText().toString(),reenterPasswordBox.getText().toString());
			 if (passwordBox.getText().toString().equals(reenterPasswordBox.getText().toString()))
			 {
				 //TODO

				 postdata(emailBox.getText().toString(), passwordBox.getText().toString());
				 CREDENTIALS.add(emailBox.getText().toString() + ":" + passwordBox.getText().toString());
			 }
				 
			 else {
				 Toast.makeText(LoginActivity.this, "Passwords do not match", Toast.LENGTH_SHORT).show();
				 passwordBox.getText().clear();
				 reenterPasswordBox.getText().clear();
				 registerDialog();
			 }
			 
		 }
		 });
		 
		// Cancel button
		 alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		 public void onClick(DialogInterface dialog, int whichButton) {
		 Toast.makeText(LoginActivity.this, "Nothing added!", Toast.LENGTH_SHORT).show();
		 }
		 });
		 
		 alert.show();
	}
	
	
	//Do not allow access to map unless login credentials verified
	@Override
	public void onBackPressed() {
		Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(setIntent);
		return;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}
		// Reset errors.
		currentUserNameView.setError(null);
		currentPassView.setError(null);
		// Store values at the time of the login attempt.
		currentUserName = currentUserNameView.getText().toString();
		currentPass = currentPassView.getText().toString();
		boolean cancel = false;
		View focusView = null;
		// Check for a valid password.
		if (TextUtils.isEmpty(currentPass)) {
			currentPassView.setError(getString(R.string.error_field_required));
			focusView = currentPassView;
			cancel = true;
		} 
		/*else if (currentPass.length() < 4) {
			currentPassView.setError(getString(R.string.error_invalid_password));
			focusView = currentPassView;
			cancel = true;
		}*/
		// Check for a valid email address.
		if (TextUtils.isEmpty(currentUserName)) {
			currentUserNameView.setError(getString(R.string.error_field_required));
			focusView = currentUserNameView;
			cancel = true;
		} 
		/*else if (!currentUserName.contains("@")) {
			currentUserNameView.setError(getString(R.string.error_invalid_email));
			focusView = currentUserNameView;
			cancel = true;
		}*/
		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute("http://gaelab110task2.appspot.com/product");
		}
	}
	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);
			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<String, Void, List<String>> {
		@Override
		protected List<String> doInBackground(String... url) {
			
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet("http://gaelab110task2.appspot.com/product");
			List<String> nameList = new ArrayList<String>();
			List<String> placeList = new ArrayList<String>();
			try {
				HttpResponse response = client.execute(request);
				HttpEntity entity = response.getEntity();
				
				String data = EntityUtils.toString(entity);
				Log.d("Error", data);
				JSONObject myjson;
				// Simulate network access.
				try {
					myjson = new JSONObject(data);
					JSONArray array = myjson.getJSONArray("data");
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = array.getJSONObject(i);
						nameList.add(obj.get("name").toString());	
						placeList.add(obj.get("description").toString());
					}
					
				} catch (JSONException e) {
					
			    	Log.d("Error", "Error in parsing JSON");
			    	return null;
				}
				
			} 
			catch (ClientProtocolException e) {
				
		    	Log.d("Error", "ClientProtocolException while trying to connect to GAE");
			} catch (IOException e) {
	
				Log.d("Error", "IOException while trying to connect to GAE");
			}
			
			ArrayList<String> pieces = new ArrayList<String>();
			
			for(int i=0; i<nameList.size(); i++){
				
			}
			String [] tempCredentials = {"",""};
			for (int i=0; i<nameList.size(); i++){
				tempCredentials = nameList.get(i).split(":");
				
				if (tempCredentials[0].equals(currentUserName) && tempCredentials[1].equals(currentPass)) {
					if (placeList.get(i).length() < 1)
					{
						MainActivity.currentAccount = new Account(currentUserName, currentPass, null);
						TrackerService.trackedAccount = new Account(currentUserName, currentPass, null);
					}
					else
					{
						MainActivity.currentAccount = new Account(currentUserName, currentPass, placeList.get(i));
						TrackerService.trackedAccount = new Account(currentUserName, currentPass, placeList.get(i));
					}
					return nameList;
				
				}
				
			}
			return null;
			// TODO: register the new account here.
			
		}
		@Override
		protected void onPostExecute(List<String> list) {
			mAuthTask = null;
			showProgress(false);
			if (list != null) {
				CREDENTIALS = (ArrayList<String>) list;
		    	 for(String temp : CREDENTIALS)
		    	 {
		    		 Log.d(temp, "CREDENTIALS");
		    	 }
				finish();
			} else {
				currentPassView
						.setError(getString(R.string.error_incorrect_password));
				currentPassView.requestFocus();
			}
		}
		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
		
	}
	
	private void postdata(final String user, final String pass) {
		final ProgressDialog dialog = ProgressDialog.show(this,
				"Creating Account...", "Please wait...", false);
		Thread t = new Thread() {
			

			
			public void run() {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost("http://gaelab110task2.appspot.com/product");
				
			    try {
			    	
			    	//Create string for test placeit description
			    	
			      List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			      nameValuePairs.add(new BasicNameValuePair("name",
			    		  user+":"+pass));
			      //TODO HARDCODED TEST ACCOUNT as JSONSTRING

			      
			      nameValuePairs.add(new BasicNameValuePair("description",
			      		 null));
			     
			      nameValuePairs.add(new BasicNameValuePair("action",
				          "put"));
			      
			      post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			    		      
			      
			      HttpResponse response = client.execute(post);
			      BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			      String line = "";
			      while ((line = rd.readLine()) != null) {
			        Log.d("TAG", line);
			      }

			    } catch (IOException e) {
			    	Log.d("TAG", "IOException while trying to conect to GAE");
			    }
				dialog.dismiss();
			}
		};
		

		t.start();
		dialog.show();
	}

}