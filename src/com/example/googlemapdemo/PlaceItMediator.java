package com.example.googlemapdemo;
/*
 *  Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * class: MainActivity.java
 * description: This class supports the mediator for PlaceIts
 * 
 */
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.google.android.gms.maps.model.Marker;

public class PlaceItMediator implements PlaceItObserver{

	private Map<AbstractPlaceIt, Marker> p2m = new HashMap<AbstractPlaceIt, Marker>();
	private Map<Marker, AbstractPlaceIt> m2p = new HashMap<Marker, AbstractPlaceIt>();
	private AbstractFactory placeItFactory = new SimplePlaceItFactory();
	

	
	public AbstractPlaceIt getPlaceIt(Marker marker){
		return m2p.get(marker);
	}
	
	public void update(String title, String description){
		//this.title = title;
		//this.snippet = description;
	}
	
	 
}
