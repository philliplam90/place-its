/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: HttpTestActivity
 * Description: This class is designed to pass a String variable named type, read in from input from the user.
 *              It will then send that string to google places in order to receive numerous places of that
 *              category.
 */


package com.example.googlemapdemo;

/**
 * Based on: http://www.taywils.me/2013/05/06/androidgoogleplacestutorial.html
	Modified by Phillip Lam 3/9/2104
 */

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class HttpTestActivity extends Activity {
   public final static String PLACESSIZE = "com.example.googlemapdemo.PLACESSIZE";
   private String latitude;
   private String longitude;
   private final String APIKEY = "AIzaSyBd67CM59eTJtXnBR1-El3tOqkN2Zweh4g"; 
   private final int radius = 2000;
   private String type;
   private StringBuilder query = new StringBuilder();
   private ArrayList<Place> places = new ArrayList<Place>();
   private int typeCount;

   @Override
   public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       Intent intent = getIntent();
       type = intent.getStringExtra("CATEGORY");
       double lat, lng;
       lat = intent.getDoubleExtra("LATITUDE", 0);
       lng = intent.getDoubleExtra("LONGITUDE", 0);
       latitude = String.valueOf(lat);
       longitude = String.valueOf(lng);
       new GetCurrentLocation().execute(latitude, longitude);

       
   }

   public static Document loadXMLFromString(String xml) throws Exception {
       DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
       DocumentBuilder builder = factory.newDocumentBuilder();

       InputSource is = new InputSource(new StringReader(xml));

       return builder.parse(is);
   }

   private class GetCurrentLocation extends AsyncTask<Object, String, Boolean> {

       @Override
       protected Boolean doInBackground(Object... myLocationObjs) {
           if(null != latitude && null != longitude) {
               return true;
           } else {
               return false;
           }
       }

       @Override
       protected void onPostExecute(Boolean result) {
           super.onPostExecute(result);
           assert result;
         
	           query.append("https://maps.googleapis.com/maps/api/place/nearbysearch/xml?");
	           query.append("location=" +  latitude + "," + longitude + "&");
	           query.append("radius=" + radius + "&");
	           query.append("types=" + type + "&");
	           query.append("sensor=true&"); //Must be true if queried from a device with GPS
	           query.append("key=" + APIKEY);
	    
           new QueryGooglePlaces().execute(query.toString());
           
       }
   }

   /**
    * Based on: http://stackoverflow.com/questions/3505930
    */
   private class QueryGooglePlaces extends AsyncTask<String, String, String> {

       @Override
       protected String doInBackground(String... args) {
           HttpClient httpclient = new DefaultHttpClient();
           HttpResponse response;
           String responseString = null;
           try {
               response = httpclient.execute(new HttpGet(args[0]));
               StatusLine statusLine = response.getStatusLine();
               if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                   ByteArrayOutputStream out = new ByteArrayOutputStream();
                   response.getEntity().writeTo(out);
                   out.close();
                   responseString = out.toString();
               } else {
                   //Closes the connection.
                   response.getEntity().getContent().close();
                   throw new IOException(statusLine.getReasonPhrase());
               }
           } catch (ClientProtocolException e) {
               Log.e("ERROR", e.getMessage());
           } catch (IOException e) {
               Log.e("ERROR", e.getMessage());
           }
           return responseString;
       }

       @Override
       protected void onPostExecute(String result) {
           super.onPostExecute(result);
           try {
               Document xmlResult = loadXMLFromString(result);
               NodeList nodeList =  xmlResult.getElementsByTagName("result");
               for(int i = 0, length = nodeList.getLength(); i < length; i++) {
                   Node node = nodeList.item(i);
                   if(node.getNodeType() == Node.ELEMENT_NODE) {
                       Element nodeElement = (Element) node;
                       Place place = new Place();
                       Node name = nodeElement.getElementsByTagName("name").item(0);
                       Node vicinity = nodeElement.getElementsByTagName("vicinity").item(0);
                       Node rating = nodeElement.getElementsByTagName("rating").item(0);
                       Node reference = nodeElement.getElementsByTagName("reference").item(0);
                       Node id = nodeElement.getElementsByTagName("id").item(0);
                       Node geometryElement = nodeElement.getElementsByTagName("geometry").item(0);
                       NodeList locationElement = geometryElement.getChildNodes();
                       Element latLngElem = (Element) locationElement.item(1);
                       Node lat = latLngElem.getElementsByTagName("lat").item(0);
                       Node lng = latLngElem.getElementsByTagName("lng").item(0);
                       float[] geometry =  {Float.valueOf(lat.getTextContent()),
                               Float.valueOf(lng.getTextContent())};
                       typeCount = nodeElement.getElementsByTagName("type").getLength();
                       String[] types = new String[typeCount];
                       for(int j = 0; j < typeCount; j++) {
                           types[j] = nodeElement.getElementsByTagName("type").item(j).getTextContent();
                       }
                       place.setVicinity(vicinity.getTextContent());
                       place.setId(id.getTextContent());
                       place.setName(name.getTextContent());
                       if(null == rating) {
                           place.setRating(0.0f);
                       } else {
                           place.setRating(Float.valueOf(rating.getTextContent()));
                       }
                       place.setReference(reference.getTextContent());
                       place.setGeometry(geometry);
                       place.setTypes(types);
                       places.add(place);
                   }
               }

           } catch (Exception e) {
               Log.e("ERROR", e.getMessage());
           }
           new addPlaces().execute(places);
       }
   }
   private class addPlaces extends AsyncTask<Object, ArrayList<Place>, Boolean> {

	    Intent intent = new Intent();
	   
		@Override
		protected Boolean doInBackground(Object... params) {
			try {
				Place place;
	    		   intent.putExtra(PLACESSIZE, places.size());
	    		   //CategoricalPlaceIt pi = new CategoricalPlaceIt();
	    		   for(int i = 0; i < places.size(); i++){
	    			   place = places.get(i);
	    			   intent.putExtra("Lat"+i, place.getGeometry()[0]);
	    			   intent.putExtra("Lng"+i, place.getGeometry()[1]);
	    			   intent.putExtra("Name"+i, place.getName());
	    		   }
			}
			finally{}
			return true;
		}
		@Override
	       protected void onPostExecute(Boolean log) {
	           super.onPostExecute(log);
	    		   setResult(Activity.RESULT_OK, intent);
	    		   finish();
	           }
   }
}

