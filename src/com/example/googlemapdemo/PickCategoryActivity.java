/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: PickCategoryActivity
 * Description: This activity is meant to utilize a drop down list for the user to choose different categories
 *              from.  It will also save the user's selection and handle it accordingly.
 */


package com.example.googlemapdemo;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class PickCategoryActivity extends Activity {

	public final static String CATEGORYMADE = "com.example.googlemapdemo.PLACEITMADE";
	public final static String CATEGORY1 = "com.example.googlemapdemo.CATEGORY1";
	public final static String CATEGORY2 = "com.example.googlemapdemo.CATEGORY2";
	public final static String CATEGORY3 = "com.example.googlemapdemo.CATEGORY3";
	final Context context = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Pick categories");
		// Create Linear Layout
		final LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		
		// select day drop down list
		 final Spinner spinner = new Spinner(context);
		 final Spinner spinner2 = new Spinner(context);
		 final Spinner spinner3 = new Spinner(context);
		 final List <String> arr = new ArrayList<String>();
		 arr.add("food");
		 arr.add("gym");
		 arr.add("school");
		 arr.add("market");
		 arr.add("library");
		 arr.add("park");
		 arr.add("coffee");
		 final ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>
		 (this, R.layout.day_list_item,arr);
		 final EditText typeCategoryArray1 = new EditText(context);
		 
		 typeCategoryArray1.setHint("category");
		 typeCategoryArray1.setFocusable(false);
		 typeCategoryArray1.setKeyListener(null);
		 typeCategoryArray1.setOnClickListener(new View.OnClickListener() 
		 {
			@Override
			public void onClick(View v) 
			{
				layout.removeViewAt(0);
				layout.addView(spinner,0);
				dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinner.setAdapter(dayAdapter);
				spinner.performClick();
	
			}
		 });
		 final EditText typeCategoryArray2 = new EditText(context);
		 typeCategoryArray2.setHint("category");
		 typeCategoryArray2.setFocusable(false);
		 typeCategoryArray2.setKeyListener(null);
		 typeCategoryArray2.setOnClickListener(new View.OnClickListener() 
		 {
			@Override
			public void onClick(View v) 
			{
				layout.removeViewAt(1);
				layout.addView(spinner2,1);
				dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinner2.setAdapter(dayAdapter);
				spinner2.performClick();
	
			}
		 });
		 final EditText typeCategoryArray3 = new EditText(context);
		 typeCategoryArray3.setHint("category");
		 typeCategoryArray3.setFocusable(false);
		 typeCategoryArray3.setKeyListener(null);
		 typeCategoryArray3.setOnClickListener(new View.OnClickListener() 
		 {
			@Override
			public void onClick(View v) 
			{
				layout.removeViewAt(2);
				layout.addView(spinner3,2);
				dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinner3.setAdapter(dayAdapter);
				spinner3.performClick();
	
			}
		 });
		 layout.addView(typeCategoryArray1,0);
		 layout.addView(typeCategoryArray2,1);
		 layout.addView(typeCategoryArray3,2);
		 alert.setView(layout);
		
		
		alert.setPositiveButton("Create", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent();
				intent.putExtra(CATEGORYMADE, true);
				if(spinner.isShown())
					intent.putExtra(CATEGORY1, arr.get(spinner.getSelectedItemPosition()));
				else
					intent.putExtra(CATEGORY1, "X");
				if(spinner2.isShown())
					intent.putExtra(CATEGORY2, arr.get(spinner2.getSelectedItemPosition()));
				else
					intent.putExtra(CATEGORY2, "X");
				if(spinner3.isShown())
					intent.putExtra(CATEGORY3, arr.get(spinner3.getSelectedItemPosition()));
				else
					intent.putExtra(CATEGORY3, "X");
				setResult(Activity.RESULT_OK, intent);
				finish();
			}
		});
		alert.show();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

}
