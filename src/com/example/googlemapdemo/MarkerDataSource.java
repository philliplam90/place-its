/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: MarkerDataSource
 * Description: This class is designed to handle the data storing of our markers.  This class handles
 *              adding a marker to the map, deleting a marker, and getting all current markers on the map.
 */


package com.example.googlemapdemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author javierAle
 */
public class MarkerDataSource {
    MySQLHelper dbhelper;
    SQLiteDatabase db;
    
    String[] cols = {MySQLHelper.TITLE, MySQLHelper.SNIPPET, MySQLHelper.LATITUDE, MySQLHelper.LONGITUDE};

    public MarkerDataSource(Context c) {
        dbhelper = new MySQLHelper(c);
        
    }
    
    public void open() throws SQLException{
         db = dbhelper.getWritableDatabase();
    }
    
    public void close(){
        db.close();
    }
    
    public void addMarker(LocationPlaceIt m){
    	double latitude = m.getLatitude();
    	double longitude = m.getLongitude();
    	
        ContentValues v = new ContentValues();
        
        v.put(MySQLHelper.TITLE, m.getTitle());
        v.put(MySQLHelper.SNIPPET, m.getDescription());
        v.put(MySQLHelper.LATITUDE, latitude);
        v.put(MySQLHelper.LONGITUDE, longitude);
        
        db.insert(MySQLHelper.TABLE_NAME, null, v);
        
    }
    
    public List<LocationPlaceIt> getMyMarkers(){
        List<LocationPlaceIt> markers = new ArrayList<LocationPlaceIt>();
        
        Cursor cursor = db.query(MySQLHelper.TABLE_NAME, cols, null, null, null, null, null);
        
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {            
            LocationPlaceIt m = cursorToMarker(cursor);
            markers.add(m);
            cursor.moveToNext();
        }
        cursor.close();
        
        
        return markers;
    }
    
    public void deleteMarker(LocationPlaceIt m){
        db.delete(MySQLHelper.TABLE_NAME, MySQLHelper.LATITUDE + " = '" + m.getLatitude() + "'", null);
        db.delete(MySQLHelper.TABLE_NAME, MySQLHelper.LONGITUDE + " = '" + m.getLongitude() + "'", null);
    }


    private LocationPlaceIt cursorToMarker(Cursor cursor) {
        LocationPlaceIt m = new LocationPlaceIt();
        m.setTitle(cursor.getString(0));
        m.setDescription(cursor.getString(1));
        
        double latitude = Double.parseDouble(cursor.getString(2));
        double longitude = Double.parseDouble(cursor.getString(3));
        
        m.setLatitude(latitude);
        m.setLongitude(longitude);
 
        return m;
    }
    
    
    
}
