package com.example.googlemapdemo;
public interface IFactorySubject {
	public void registerObserver(IFactoryObserver obs);
	public void removeObserver(IFactoryObserver obs);
	public void notifyObservers(AbstractPlaceIt pi);
}