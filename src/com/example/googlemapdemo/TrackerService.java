package com.example.googlemapdemo;

import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;

public class TrackerService extends Service {
public static final String BROADCAST_ACTION = "Hello World";
public static Account trackedAccount = null;
private static final int TWO_MINUTES = 1000 * 60 * 2;
public LocationManager locationManager;
public MyLocationListener listener;
public Location previousBestLocation = null;
LocationRequest mLocationRequest;
boolean mUpdatesRequested;
Context context = this;
MarkerDataSource data;
Calendar c = Calendar.getInstance();
int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
Intent intent;
int counter = 0;
List<LocationPlaceIt> ourList;
@Override
public void onCreate() {
    super.onCreate();
    intent = new Intent(BROADCAST_ACTION);      
    
}

@Override
public void onStart(Intent intent, int startId) {  
	Toast.makeText(this, "location changed: onStart", Toast.LENGTH_SHORT).show();

   	  trackedAccount.remakePlaceIts();
   	  Log.d("REMADE MARKERS", "YES");
      ourList = trackedAccount.getLocationPlaceIts();
      Log.d("GOTLIST", "YES");
      locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
      listener = new MyLocationListener();        
      locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 4000, 0, listener);
      locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 4000, 0, listener);
  

}


@Override
public IBinder onBind(Intent intent) {
    return null;
}

protected boolean isBetterLocation(Location location, Location currentBestLocation) {
    if (currentBestLocation == null) {
        // A new location is always better than no location
        return true;
    }

    // Check whether the new location fix is newer or older
    long timeDelta = location.getTime() - currentBestLocation.getTime();
    boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
    boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
    boolean isNewer = timeDelta > 0;

    // If it's been more than two minutes since the current location, use the new location
    // because the user has likely moved
    if (isSignificantlyNewer) {
        return true;
    // If the new location is more than two minutes older, it must be worse
    } else if (isSignificantlyOlder) {
        return false;
    }

    // Check whether the new location fix is more or less accurate
    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
    boolean isLessAccurate = accuracyDelta > 0;
    boolean isMoreAccurate = accuracyDelta < 0;
    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

    // Check if the old and new location are from the same provider
    boolean isFromSameProvider = isSameProvider(location.getProvider(),
            currentBestLocation.getProvider());

    // Determine location quality using a combination of timeliness and accuracy
    if (isMoreAccurate) {
        return true;
    } else if (isNewer && !isLessAccurate) {
        return true;
    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
        return true;
    }
    return false;
}

/** Checks whether two providers are the same */
private boolean isSameProvider(String provider1, String provider2) {
    if (provider1 == null) {
      return provider2 == null;
    }
    return provider1.equals(provider2);
}

@Override
public void onDestroy() {       
   // handler.removeCallbacks(sendUpdatesToUI);     
    super.onDestroy();
    Log.v("STOP_SERVICE", "DONE");
    locationManager.removeUpdates(listener);        
}   

public static Thread performOnBackgroundThread(final Runnable runnable) {
    final Thread t = new Thread() {
        @Override
        public void run() {
            try {
                runnable.run();
            } finally {

            }
        }
    };
    t.start();
    return t;
}
public void getNotification(String mTitle)
	{
		long[] vibpattern ={100, 200, 100, 500};
		Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(this)
		        .setSmallIcon(R.drawable.yellow_square)
		        .setContentTitle(mTitle)
		        .setContentText("You got close to a Place-it")
		        .setVibrate(vibpattern)
		        .setSound(notification);
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(this, MainActivity.class);
		resultIntent.setAction(Intent.ACTION_MAIN);
	    resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);

	    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
	            resultIntent, 0);
	    mBuilder.setContentIntent(pendingIntent);

		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MainActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
		    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		int mId=0;
	// mId allows you to update the notification later on.
		mNotificationManager.notify(mId, mBuilder.build());
		
	}

public class MyLocationListener implements LocationListener
{
	boolean errFlag = false;
    public void onLocationChanged(final Location location)
    {
    	
		Log.d("Actually Checking", "YES");
		double currentLatitude =location.getLatitude();
		double currentLongitude = location.getLongitude();
		String notify = null;
		double radius =6371;
		double halfMile = 0.804672;
	    
	       //need this for loop  
	       for (int index = 0; index < ourList.size(); index++) 
			{
				double latitude = ourList.get(index).getLatitude();
				double longitude = ourList.get(index).getLongitude();
				
				
				
				 double markerlatitude = latitude;
		            double markerlongitude = longitude;     
			
					double dLat = Math.toRadians(markerlatitude-currentLatitude);
					double dLon = Math.toRadians(markerlongitude-currentLongitude);
					double a = Math.pow(Math.sin(dLat/2),2)+ 
							   Math.cos(Math.toRadians(currentLatitude))*Math.cos(Math.toRadians(markerlatitude))
							   *Math.sin(dLon/2)*Math.sin(dLon/2);
					double c = 2*Math.asin(Math.sqrt(a));
					double valueResult = radius*c;
					double km = valueResult/1;
					
					if(km< halfMile)
					{       if(errFlag == false){
							//notify = m.get(i).getTitle();
							getNotification(notify);
							errFlag = true;
					         }
					     
					}
					else
						errFlag = false;
					/*if(notify!=null && m.get(i).getDay()== dayOfWeek && m.get(i).getCallFlag()==true) //removed isFlat check
					{
						m.get(i).setCallFlag(false);
						getNotification(notify);
					}*/
				
				// call to create a place it
				// add it to "abstractPlaceIts" and locationPlaceIts list
			
			}
    

    }

	public void onProviderDisabled(String provider)
    {
        Toast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();
    }


    public void onProviderEnabled(String provider)
    {
        Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
    }


    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

}
}