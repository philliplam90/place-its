/*
 * Author: Team 23
 * Team 23: Chris Chan, Elmer Yang, Kevin Chaves, Michael Furuya, Phillip Lam, Sean Kim
 * Class Name: CreatePlaceItActivity
 * Description: This activity is meant to create a place-it when the user clicks on the map.  The user
 *              will then be prompted to provide some information such as the name of the place-it, description,
 *              time, and day of the week.  This activity will save all of the user's configurations and
 *              create a place-it on the map.
 */


package com.example.googlemapdemo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class CreatePlaceItActivity extends Activity {
	public final static String TITLE = "com.example.googlemapdemo.TITLE";
	public final static String DESCRIPTION = "com.example.googlemapdemo.DESCRIPTION";
	public final static String PLACEITMADE = "com.example.googlemapdemo.PLACEITMADE";
	public final static String DAYOFWEEK = "com.example.googlemapdemo.DAYOFWEEK";
	Calendar mcurrentTime = Calendar.getInstance();
    int time_hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
    int time_minute = mcurrentTime.get(Calendar.MINUTE);
    String timeString = " ";
    final Context context = this;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {       
        super.onCreate(savedInstanceState);
        
    }
	@Override
	protected void onStart() {
		super.onStart();
		// Show the Up button in the action bar.
		setupActionBar();
		 AlertDialog.Builder alert = new AlertDialog.Builder(this);
		 alert.setTitle("New Place-it");
		
		 // Create Linear Layout
		 final LinearLayout layout = new LinearLayout(context);
		 layout.setOrientation(LinearLayout.VERTICAL);
		 
		 // Set an EditText view to get user input: Title and Description 
		 final EditText titleBox = new EditText(this);
		 titleBox.setHint("Title");
		 layout.addView(titleBox);
		 
		 // edit description
		 final EditText descriptionBox = new EditText(context);
		 descriptionBox.setHint("Description");
		 layout.addView(descriptionBox);
		
		 // time option
		 final EditText timeBox = new EditText(context);
		 timeBox.setHint("Choose a Time");
		 timeBox.setKeyListener(null);
		 timeBox.setFocusable(false);
		 layout.addView(timeBox);
		 timeBox.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
								
		        TimePickerDialog mTimePicker;
		        mTimePicker = new TimePickerDialog(CreatePlaceItActivity.this, new TimePickerDialog.OnTimeSetListener() {
		            
		        	@Override
		            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
		        		time_hour = selectedHour;
		                time_minute = selectedMinute;
		        		convertAndSetTime(timeBox, time_hour, time_minute);
		            }
		        }, time_hour, time_minute, false);//No 24 hour time
		        mTimePicker.setTitle("Select A Time");
		        mTimePicker.show();
		   	}
		 });
		 
		 // select day drop down list
		 final Spinner spinner = new Spinner(context);
		 final List <String> arr = new ArrayList<String>();
		 arr.add("Sun");
		 arr.add("Mon");
		 arr.add("Tue");
		 arr.add("Wed");
		 arr.add("Thur");
		 arr.add("Fri");
		 arr.add("Sat");
		 final ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>
		 (this, R.layout.day_list_item,arr);
		 final EditText dayViewArray = new EditText(context);
		 dayViewArray.setHint("Day(s) to remind");
		 dayViewArray.setFocusable(false);
		 dayViewArray.setKeyListener(null);
		 layout.addView(dayViewArray);
		 dayViewArray.setOnClickListener(new View.OnClickListener() 
		 {
			@Override
			public void onClick(View v) 
			{
				layout.removeView(dayViewArray);
				layout.addView(spinner);
				dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinner.setAdapter(dayAdapter);
				spinner.performClick();

			}
		 });
		 //
		 alert.setView(layout);
		 
		 // Create button and instantiation of PlaceIt
		 alert.setPositiveButton("Create", new DialogInterface.OnClickListener() {
		 public void onClick(DialogInterface dialog, int whichButton) {
			 if(spinner.getSelectedItemPosition() < 0)
				 Toast.makeText(CreatePlaceItActivity.this, "Nothing added! \n\t\t\t Set day!", Toast.LENGTH_SHORT).show();
			 else
			 {
				 String title = titleBox.getText().toString();
				 String description = descriptionBox.getText().toString() +"\n" +
			 				arr.get(spinner.getSelectedItemPosition()) + " " + timeString;
				 Toast.makeText(CreatePlaceItActivity.this, "Place-it created!", Toast.LENGTH_SHORT).show();
				 Intent intent = new Intent();
				 intent.putExtra(PLACEITMADE, true);
				 intent.putExtra(TITLE, title);
				 intent.putExtra(DESCRIPTION, description);
				 intent.putExtra(DAYOFWEEK, spinner.getSelectedItemPosition()+1);
				 setResult(Activity.RESULT_OK, intent);
				 finish();
			 }
		 }
		 });
		 // Cancel button
		 alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			 public void onClick(DialogInterface dialog, int whichButton) {
			 Toast.makeText(CreatePlaceItActivity.this, "Nothing added!", Toast.LENGTH_SHORT).show();
			 finish();
			 }
		 });
		 alert.show();
	}

	public void onStop(){
		super.onStop();
	}
	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_place_it, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	 /* Converts 24h time to 12h time and sets it to the text of the EditText parameter */
	public void convertAndSetTime (EditText edtxt, int hr, int min) {
		
		String am_pm = "am";
       time_hour = hr;
       time_minute = min;
       if (hr > 12){
       	time_hour = hr - 12;
       	am_pm = "pm";
       }
       else if (hr == 12) {
       	time_hour = hr;
       	am_pm = "pm";
       }
       else if (hr == 0) {
       	time_hour = 12;
       }
       if (min < 10) {
       	String small_minute = "0" + min;
       	timeString = time_hour + ":" + small_minute + am_pm;
       	edtxt.setText( timeString );
       }
       else {
       	timeString = time_hour + ":" + time_minute + am_pm;
       	edtxt.setText( timeString );
       }
	}
}
