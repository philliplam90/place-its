package com.example.googlemapdemo;


import java.util.Calendar;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

@SuppressLint("ParcelCreator")
public class PlaceIt implements Parcelable
{
	private Marker marker;
	final Calendar c = Calendar.getInstance();
	private int day;
	private int hour;
	private int minute;
	
	private LatLng pos;
	private String title;
	private String desc;
	private boolean tempCallFlag = true; //temporary until we set 45min callback on placit
										 // checks if placeit has already been shown to user
	
	
	public PlaceIt(){};
	
	//PlaceIt Parcel Constructor
	public PlaceIt(Parcel in)
	{
		day=in.readInt();
		pos=in.readParcelable(LatLng.class.getClassLoader());
		title=in.readString();
		desc=in.readString();
	}
	public PlaceIt(int d, LatLng p, String t, String de)
	{
		day=d;
		pos=p;
		title=t;
		desc=de;
	}
	public PlaceIt(Marker mark, int d)
	{
		marker = mark;
		day = d;
		
		pos = marker.getPosition();
		title = marker.getTitle();
		desc = marker.getSnippet();
	}
	public PlaceIt(Marker mark, int d, int h, int m)
	{
		marker = mark;
		day = d;
		hour = h;
		minute = m;
	}

	public Marker getMarker()
	{
		return marker;
	}
	
	public int getDay()
	{
		return day;
	}
	public LatLng getPosition()
	{
		return pos;
	}
	public String getTitle()
	{
		return title;
	}
	public String getDescription()
	{
		return desc;
	}
	public boolean getCallFlag()
	{
		return tempCallFlag;
	}
	public void setCallFlag(boolean c)
	{
		tempCallFlag = c;
	}
	
	/*
	 * Implement Parcelable methods and 
	 * instantiate CREATOR object to serialize 
	 * PlaceIts objects into byte arrays
	 */
	public static final Parcelable.Creator<PlaceIt> CREATOR
	    = new Parcelable.Creator<PlaceIt>() {
	public PlaceIt createFromParcel(Parcel in) {
	    return new PlaceIt(in);
	}
	
	public PlaceIt[] newArray(int size) {
	    return new PlaceIt[size];
	}
	};

	@Override
	public int describeContents() 
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel out, int arg1) 
	{
		out.writeInt(day);
		out.writeParcelable(pos , 0);
		out.writeString(title);
		out.writeString(desc);
	}
}