# view res/layout/activity_main.xml #generated:3
-keep class android.support.v4.widget.DrawerLayout { <init>(...); }

# view AndroidManifest.xml #generated:71
-keep class com.example.googlemapdemo.CreatePlaceItActivity { <init>(...); }

# view AndroidManifest.xml #generated:88
-keep class com.example.googlemapdemo.GCMIntentService { <init>(...); }

# view AndroidManifest.xml #generated:79
-keep class com.example.googlemapdemo.HttpTestActivity { <init>(...); }

# view AndroidManifest.xml #generated:66
-keep class com.example.googlemapdemo.LoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:48
-keep class com.example.googlemapdemo.MainActivity { <init>(...); }

# view AndroidManifest.xml #generated:108
-keep class com.example.googlemapdemo.PickCategoryActivity { <init>(...); }

# view AndroidManifest.xml #generated:105
-keep class com.example.googlemapdemo.RegisterActivity { <init>(...); }

# view AndroidManifest.xml #generated:42
-keep class com.example.googlemapdemo.TrackerService { <init>(...); }

# view AndroidManifest.xml #generated:90
-keep class com.google.android.gcm.GCMBroadcastReceiver { <init>(...); }

# view res/layout/activity_main.xml #generated:15
-keep class com.google.android.gms.maps.MapFragment { <init>(...); }

# onClick res/layout/activity_main.xml #generated:37
-keepclassmembers class * { *** geoLocate(...); }

